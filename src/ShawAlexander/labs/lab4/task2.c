/*
2. Написать программу, которая с помощью массива указателей выводит слова строки в обратном порядке
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SIZE 256

int main() {
    char string[256];
    //Массив указателей содержит адреса начала и конца каждого слова
    char *pointers[SIZE / 2][2] = { { 0 }, { 0 } };
    char startFound = 0;
    int start = -1, words = 0;
    fgets(string, SIZE, stdin);
    for (int i = 0; i <= strlen(string); i++)
    {
        char c = string[i];
        if (!isspace(c) && startFound == 0) {
            start = i;
            startFound = 1;
        }
        else if ((isspace(c) || c == 0) && startFound == 1) {
            pointers[words][0] = &string[start];
            pointers[words][1] = &string[i];
            startFound = 0;
            words++;
        }
    }
    words--;
    while (words >= 0) {
        while (pointers[words][0] < pointers[words][1])
        {
            printf("%c", *pointers[words][0]);
            pointers[words][0]++;
        }
        putchar(' ');
        words--;
    }
    putchar('\n');
    return 0;
}