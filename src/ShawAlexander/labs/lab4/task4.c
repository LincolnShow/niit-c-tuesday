//4. Изменить программу дл€ поиска самых длинных последовательностей в массиве с использованием указателей вместо числовых переменных

#include <stdio.h>
#include <ctype.h>
#define SIZE 256
int main() {
    char string[SIZE] = { 0 };
    char *longestSeq[2];
    char *stringPtr = string;

    puts("Enter a string:");
    fgets(string, SIZE, stdin);

    char c, lastChar;
    short startFound = 0;
    int start = 0, longestSize = 0;
    for (int i = 0; c = *stringPtr; i++, stringPtr++)
    {
        if (!startFound && !isspace(c)) {
            startFound = 1;
            start = i;
        }
        else if (startFound && (c != lastChar)) {
            startFound = 0;
            if (i - start > longestSize) {
                longestSize = i - start;
                longestSeq[0] = &string[start];
                longestSeq[1] = &string[i];
                i--;
                stringPtr--;
            }
        }
        lastChar = c;
    }
    printf("%d ", longestSize);
    while (longestSeq[0] < longestSeq[1]) {
        putchar(*longestSeq[0]);
        longestSeq[0]++;
    }
    putchar('\n');
    return 0;
}