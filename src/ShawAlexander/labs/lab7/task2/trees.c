#include "trees.h"
void chomp(char *string) {
    string[strlen(string) - 1] = 0;
}
PNODE getNewNode() {
    PNODE node = (PNODE)malloc(sizeof(TNODE));
    node->keyword = (char*)malloc(sizeof(char) * 16);
    strcpy(node->keyword, "");
    node->timesUsed = 0;
    node->left = NULL;
    node->right = NULL;
    return node;
}
PNODE makeTree(FILE *keyFile) {
    int lines = 0;
    int c = 0;
    while (EOF != (c = fgetc(keyFile))) {
        if(c == '\n')
            lines++;
    }
    rewind(keyFile);
    int i = 0;
    char **pwords = (char**)malloc(sizeof(char*) * lines);
    for (int q = 0; q < lines; q++) {
        pwords[q] = malloc(sizeof(char) * 16);
    }
    while (fgets(pwords[i], 16, keyFile)) {
        chomp(pwords[i]);
        i++;
    }
    PNODE root = getNewNode();
    strcpy(root->keyword, pwords[lines / 2]);
    insertInOrder(root, pwords, lines / 4, lines / 2);
    insertWord(root, pwords[0]);
    for (int q = 0; q < lines; q++) {
        free(pwords[q]);
    }
    free(pwords);
    return root;
}
void insertInOrder(PNODE root, char **pwords, int pow, int elem) {
    insertWord(root, pwords[elem]);
    if (pow > 0) {
        insertInOrder(root, pwords, pow / 2, elem - pow);
    }
    if (pow > 0) {
        insertInOrder(root, pwords, pow / 2, elem + pow);
    }
}
PNODE insertWord(PNODE root, char *kword) {
    if (!root) {
        PNODE word = getNewNode();
        strcpy(word->keyword, kword);
        return word;
    }
    else {
        if (!strcmp(kword, root->keyword))
            return NULL;
        char i = 0;
        while (kword[i] == root->keyword[i])
            i++;
        if (kword[i] < root->keyword[i]) {
            if(!root->left)
                root->left = insertWord(root->left, kword);
            else
                insertWord(root->left, kword);
        }
        else {
            if (!root->right)
                root->right = insertWord(root->right, kword);
            else
                insertWord(root->right, kword);
        }

    }
}
PNODE searchTree(PNODE root, char *string) {
    static PNODE out = NULL;
    if (out != NULL && strcmp(out->keyword, string))
        out = NULL;
    if (out != NULL)
        return out;
    if (root) {
        if (!strcmp(root->keyword, string)) {
            out = root;
        }
        else {
            if(root->left)
                searchTree(root->left, string);
            if(root->right)
                searchTree(root->right, string);
        }
    }
    return out;
}
void printTree(PNODE root) {
    if (root) {
        if (root->left)
            printTree(root->left);
        printf("%16s : %2d times\n", root->keyword, root->timesUsed);
        if (root->right)
            printTree(root->right);
    }
}