/*3. �������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ��������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����.
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#define SIZE 256
int main() {
    FILE *fp;
    FILE *fpOUT = fopen("c:\\result.txt", "w");
    char path[SIZE / 2] = { 0 };
    puts("Enter a file path in the following format (c:\\file.txt); results will be saved in c:\\result.txt:");
    fgets(path, SIZE, stdin);
    path[strlen(path) - 1] = 0;
    if ((fp = fopen(path, "r+")) == NULL)
        perror("Error");
    else {
        char c0;
        int lines = 1;
        char fileString[SIZE] = { 0 };
        while ((c0 = fgetc(fp)) != EOF) {
            if (c0 == '\n')
                lines++;
        }
        fseek(fp, 0, SEEK_SET);
        //������ �� �����
        for (int i = 0; i < lines; i++)
        {
            fgets(fileString, SIZE, fp);
            short startFound = 0, isSecond = -1;
            int start = 0;
            srand(time(0));
            //������ �� ������ � ���������� ����
            for (int q = 0; q < strlen(fileString) + 1; q++)
            {
                char c = fileString[q];
                if (!isspace(c) && !startFound && isSecond == -1) {
                    isSecond++;
                }
                else if (!isspace(c) && !startFound && isSecond == 0) {
                    startFound = 1;
                    start = q;
                    isSecond++;
                }
                else if ((isspace(c) || c == EOF || c == 0) && startFound) {
                    startFound = 0;
                    isSecond = -1;
                    int end = q;
                    char symbolsToSwap[SIZE] = { 0 };
                    //���������� ������� ���������, ������� ������������ ����� ����������
                    for (int w = start, symbol = 0; w < end - 1; w++, symbol++) {
                        symbolsToSwap[symbol] = fileString[w];
                    }
                    //������������� �������� � �������
                    for (int s = 0; s < strlen(symbolsToSwap); s++)
                    {
                        short randPos = rand() % strlen(symbolsToSwap);
                        char tmp = symbolsToSwap[s];
                        symbolsToSwap[s] = symbolsToSwap[randPos];
                        symbolsToSwap[randPos] = tmp;
                    }
                    //������ �������� � ������������ �������
                    for (int e = start, r = 0; e < end - 1; e++, r++) {
                        fileString[e] = symbolsToSwap[r];
                    }
                }
                else if (isspace(c) && !startFound) {
                    isSecond = -1;
                }
            }
            printf("%s", fileString);
            fputs(fileString, fpOUT);
        }
        putchar('\n');
    }
    fclose(fp);
    fclose(fpOUT);
    return 0;
}