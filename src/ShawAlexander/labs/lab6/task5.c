/*5. �������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����
*/
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <windows.h>
unsigned long long fibonacci(int);
int main() {
    FILE *fpOUT = fopen("c:\\fibTable.xls", "wt");
    clock_t start, end;
    unsigned long long fib = 0;
    for (int i = 1; i <= 40; i++) {
        start = clock();
        fib = fibonacci(i);
        end = clock();
	printf("%d - %llu\n", i, fib);
        fprintf(fpOUT, "%d\t%.3lf\n", i, ((double)end - start) / CLOCKS_PER_SEC);
    }
    fclose(fpOUT);
    return 0;
}
//���������� n-�� ����� ���� ���������
unsigned long long fibonacci(int n) {
    if (n == 1 || n == 2)
        return 1;
    else {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}