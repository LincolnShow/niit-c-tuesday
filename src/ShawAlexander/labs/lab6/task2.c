/*2. �������� ���������, ������� ������� � ��������� ����� ����� ��
2 �� 1000000 �����, ����������� ����� ������� �������������-
����� ��������
���������:
������������������� �������� �������� �������� ���, ������ ������� ��-
������ ����������� � ����������� �� ��������/ ���������� ����������� ��
������:
n ? 3n + 1, ���� n ��������.
n ? n/2, ���� n ������.
1
������������ ������������������ ���������� � ���������� n (� ������ �� 2
�� 1000000), � �������������, ����� n ���������� ������ 1. ���������� �����
����� �������� n, ������� ��������� ����� ������� ������������������.
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
typedef unsigned long long llu;
int getCollatzLength(llu);
long main() {
    int longestLength = 0;
    int l = 0;
    llu number = 0;
    for (int i = 2; i < 1000000; i++) {
        if ((l = getCollatzLength(i)) > longestLength) {
            longestLength = l;
            number = i;
        }
    }
    printf("Answer: %llu\nThe Longest Length: %d\n", number, longestLength);
    return 0;
}
//���������� ����� ������������������ ��������� ����� x
int getCollatzLength(llu x) {
    static int longest = 0;
    longest++;
    if (x == 1) {
        int tmp = longest;
        longest = 0;
        return tmp;
    }
    else {
        if (x % 2 == 0)
            getCollatzLength(x / 2);
        else
            getCollatzLength((x * 3) + 1);
    }
}
