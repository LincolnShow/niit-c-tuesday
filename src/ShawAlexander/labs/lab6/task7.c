#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
char **getTable(FILE*);
void copyMaze(FILE *, char**);
void printMaze(char**);
char findExit(int, int);
char check(int, int, char*);
char checkStuck(int, int);
char checkWin(int, int);
char **maze;
int x_max, y_max, x_start, y_start;
 
int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("Usage: FILE_PATH");
        return 1;
    }
    FILE *mazeFile = fopen(argv[1], "rt");
    maze = getTable(mazeFile);
    copyMaze(mazeFile, maze);
    printMaze(maze);
    findExit(y_start, x_start);
    return 0;
}
//���������� ������� � ��������, ��������������� ������� ��������� �� �����;
//������ ������ � ������ � ���������� ���������� x_max, y_max
char **getTable(FILE *mazeFile) {
    int c;
    int xlength = 0, ylength = 1;
    while ((c = fgetc(mazeFile)) != EOF) {
        if (c == '\n') {
            ylength++;
        }
        else if (ylength == 1) {
            xlength++;
        }
    }
    char **maze = malloc(ylength * sizeof(char*));
    for (int i = 0; i < ylength; i++) {
        maze[i] = malloc(sizeof(char) * xlength);
    }
    x_max = xlength;
    y_max = ylength;
    return maze;
}
//����������� ��������� �� ����� � �������; ������ ��������� ��������� ��������� � ���������� ���������� x_start, y_start;
void copyMaze(FILE *src, char **dst) {
    int c;
    int y = 0, x = 0;
    rewind(src);
    while ((c = fgetc(src)) != EOF) {
        if (c == '\n') {
            y++;
            x = 0;
        }
        else {
            if (c == 'x') {
                x_start = x;
                y_start = y;
            }
            dst[y][x] = c;
            x++;
        }
    }
}
//����� ������� �� �����
void printMaze(char **maze) {
    for (int i = 0; i < y_max; i++) {
        for (int q = 0; q < x_max; q++) {
            printf("%c", maze[i][q]);
        }
        putchar('\n');
    }
}
//����������� ������� ��� ���������� ������
char findExit(int y, int x) {
    system("cls");
    maze[y][x] = '.';
    printMaze(maze);
    Sleep(100);
    if (checkWin(y, x)) {
        printf("The exit has been found!\n");
        exit(1);
    }
    if (check(y, x, "up")) {
        findExit(y - 1, x);
    }
    if (check(y, x, "right")) {
        findExit(y, x + 1);
    }
    if (check(y, x, "down")) {
        findExit(y + 1, x);
    }
    if (check(y, x, "left")) {
        findExit(y, x - 1);
    }
    //���� ������ ������ �� � ����� �����������, �� ������� �� ��������
    if(checkStuck(y, x)) {
        return 0;
    }
}
//�������� �������� ��������� �� ��������� � ������� ������� (1 - ��������� ������������ �����; 0 - �����/���������� �����)
char check(int curY, int curX, char *place) {
    if ((place == "up" && maze[curY - 1][curX] == ' ') ||
        (place == "left" && maze[curY][curX - 1] == ' ') ||
        (place == "right" && maze[curY][curX + 1] == ' ') ||
        (place == "down" && maze[curY + 1][curX] == ' '))
        return 1;
    else
        return 0;
}
//�������� ���� �������� ��������� �� ��������� � ������� ������� �� ������������ (1 - ������ ������ �� � ����� �����������)
char checkStuck(int curY, int curX) {
    if (check(curY, curX, "up") && check(curY, curX, "left") && check(curY, curX, "right") && check(curY, curX, "down"))
        return 1;
    else 
        return 0;
}
//�������� ��������� �� ����������� ������ ��������� (1 - ����� �� �������)
char checkWin(int curY, int curX) {
    if ((curY == 0 || curY == y_max-1) ||
        (curX == 0 || curX == x_max-1))
        return 1;
    else return 0;
}