/*
3. �������� ���������, ������� ��� �������� ������ ������� ����� �������
����� � ��� �����
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 2000


int main() {
    char string[SIZE];
    short startFound = 0;
    int maxLength = 0, start = -1, end = -1;
    int longestStart = 0, longestEnd = 0;
	puts("Enter a string:");
    fgets(string, SIZE, stdin);
    for (int i = 0; i < strlen(string); i++)
    {
        char c = string[i];
        if (!isspace(c) && startFound == 0) {
            startFound = 1;
            start = i;
        }
        else if ((isspace(c) || c == ',' || c == '.') && startFound == 1) {
            end = i;
            if ((end - start) > maxLength){
                longestStart = start;
                longestEnd = end;
                maxLength = end - start;
            }
            startFound = 0;
        }
            
    }
    while (longestStart < longestEnd) {
        putchar(string[longestStart]);
        longestStart++;
    }
    printf(" : %d\n", maxLength);
    return 0;
}