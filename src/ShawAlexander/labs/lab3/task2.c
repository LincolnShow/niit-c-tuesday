/*
2. �������� ���������, ������� ��� �������� ������ ���������� ��-
�������� ���� � ������� ������ ����� �� ��������� ������ � ���
�����
*/

#include <stdio.h>
#include <ctype.h>
#define SIZE 2000

int main() {
    char string[SIZE];
	puts("Enter a string:");
    fgets(string, SIZE, stdin);
    int wordCount = 0, start, end;
    short startFound = 0;

    for (int i = 0; i < strlen(string); i++)
    {
        char c = string[i];
        if (!isspace(c) && startFound == 0) {
            startFound = 1;
            start = i;
        }
        if ((isspace(c) || c == ',' || c == '.') && startFound == 1) {
            end = i;
            wordCount++;
            for (int q = start; q < end; q++)
            {
                putchar(string[q]);
            }
            printf(" : %d letters\n", (end - start));
            startFound = 0;
        }
    }
    printf("Total words: %d\n", wordCount);
    return 0;
}