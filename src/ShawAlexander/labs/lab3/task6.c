/*
6. �������� ���������, ������� ��������� ������������� ������ �������
N
,
� ����� ������� ����� ��������� ����� ����������� � ������������ ���-
�������.
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define SIZE 10
int main() {
    srand(time(0));
    short array[SIZE];
    int positives = 0, negatives = 0;
    //���������� ������� ���������� �������
    for (int i = 0; i < SIZE; i++)
    {
        if (positives < SIZE / 2 && negatives < SIZE / 2)
        {
            int coin = rand() % 2;
            if (coin == 0) {
                array[i] = rand();
                positives++;
            }
            else {
                array[i] = -rand();
                negatives++;
            }
        }
        else if (positives >= SIZE / 2)
            array[i] = -rand();
        else if (negatives >= SIZE / 2)
            array[i] = rand();
    }
    int start = -1, end = -1, max = 0, min = 0;
    //������� ����������� � ������������ ��������
    for (int i = 0; i < SIZE; i++) {
        if (array[i] > max) {
            max = array[i];
            end = i;
        }
        if (array[i] < min) {
            min = array[i];
            start = i;
        }
    }
    int result = 0;
    if (start > end) {
        int tmp = start;
        start = end;
        end = tmp;
    }
    for (int i = start; i <= end; i++)
    {
        result += array[i];
    }
    //��������
    for (int i = 0; i < SIZE; i++)
    {
        printf("%6d\n", array[i]);
    }
    printf("MIN = %d, MAX = %d\n", min, max);
    printf("TOTAL: %d\n", result);
}