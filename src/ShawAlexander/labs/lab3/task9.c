/*
9. �������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������
AABCCCDDEEEEF
��������� 4 �
EEEE
*/

#include <stdio.h>
#include <ctype.h>
#define SIZE 2000


int main() {
	char string[SIZE];
    puts("Enter a string:");
	fgets(string, SIZE, stdin);
	int maxLength = -1, maxStart = -1, maxEnd = -1, start = -1, end = -1;
	short startFound = 0;
	char lastChar = 0;
	//������� ������ � ����� ����� ������� ������������������
	for (int i = 0; i < SIZE; i++)
	{
		char c = string[i];
		if (c == 0)
			break;
		else if (!isspace(c) && startFound == 0) {
			start = i;
			startFound = 1;
			lastChar = c;
		}
		else if ((c != lastChar && !isspace(c)) || (isspace(c) && startFound == 1)) {
			end = i;
			startFound = 0;
			if (!isspace(c) && string[i+1] != 0)
				i--;
			if (end - start > maxLength) {
				maxLength = end - start;
				maxStart = start;
				maxEnd = end;
			}

		}
	}
	//��������
	printf("%d ", maxLength);
	for (int i = maxStart; i < maxEnd; i++)
	{
		putchar(string[i]);
	}
	putchar('\n');
	return 0;
}